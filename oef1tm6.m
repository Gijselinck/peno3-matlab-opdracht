%#ok<*NOPTS>

% author: Tom Gijselinck  <tomgijselinck@gmail.com>
% date: 26/09/2014

'Oefening 1'
% 1.
sin(pi/4)
% 2.
(2+3^4)/6^(1/5)
% 3.
exp(2)
% 4.
log(abs(sinh(-(pi-1)/2)))

'Oefening 2'
x = 4;
% 1.
tan(x)/sqrt(1+x^2)
% 2.
x*exp(-2*x)

'Oefening 3'
M = [1 2; 3 4];
P = [0 -1 2; 1 0 4];
S = [M 2*M; P [8; 8]];
Z = P';
T = M*P;
Q = P.*T;
R = P.^2;
% 1.
P(:,3)
% 2.
%P(3,:) % maar 2 rijen, dus error (outcommented code)
% 3.
Z([2,3],:)
% 4.
S([1,2],[3,4])
% 5.
S(M(:,2),M(1,:));
% 6.
Ma = -T + R.*P
% 7.
Trix = R'*M

'Oefening 4'
% 1.
100:2:200
% 2.
(1:40).^5
% 3.
prod(1:25)
sum((1:200).^2)
% 4.
t = 0:0.1:1;
ft = t./(1+sqrt(t))

'Oefening 5'
% Oplossing:
% Het is voldoende om de gewone operatoren "*", "/" en "^" te vervangen met
% de overeenkomstige elementsgewijze operatoren ".*", "./" en ".^".

'Oefening 6'
A = randn(5, 5)
b = (1:5)'
x = A\b
e = eig(A)
[X,E] = eig(A)
% Antwoord:
% Bovenstaande uitdrukking geeft een diagonalde matrix E met de
% eigenwaarden van A en een volle matrix X waarvan de kolommen de
% overeenkomstige eigenvectoren zijn.
