% Oefening 8, vraag 1

%#ok<*NOPTS>

% author: Tom Gijselinck  <tomgijselinck@gmail.com>
% date: 27/09/2014

function n = snelheid(z0, c, N)
% Invoer:   Startwaarde z0
%           constante c
%           maximaal aantal iteraties N
% Uitvoer:  ontsnappingssnelheid n

n = N;
z = z0;
for i = 1:N-1
    z = z^2 + c;
    if abs(z) > 2
        %we hebben n gevonden
        n = i;
        break
    end
end