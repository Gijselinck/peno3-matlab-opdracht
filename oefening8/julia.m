% Oefening 8, vraag 2

%#ok<*NOPTS>

% author: Tom Gijselinck  <tomgijselinck@gmail.com>
% date: 27/09/2014

function M = julia(zMax,c,N)
% Invoer:   zMax is de bovengrens van re�le en imaginaire delen van all z0
%               waarvoor de ontsnappingssnelheid wordt berekend
%           constante c
%           maximaal aantal iteraties N
% Uitvoer:  matrix M met alle ontsnappingssnelheden

[A, B] = meshgrid(linspace(-zMax,zMax,500), linspace(-zMax,zMax,500));
Z = A + 1i*B;
M = zeros(500,500);
for i = 1:500
    for j = 1:500
        M(i,j) = snelheid(Z(i,j),c,N);
    end
end