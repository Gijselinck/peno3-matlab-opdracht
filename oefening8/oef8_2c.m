% Oefening 8 - 2.c

%#ok<*NOPTS>

% author: Tom Gijselinck  <tomgijselinck@gmail.com>
% date: 27/09/2014

A = julia(1,-0.3+1i*0.64,100);
B = julia(1,0.285+1i*0.01,100);
C = julia(1,-0.8+1i*0.156,100);
D = julia(1,-0.70176+1i*-0.3842,100);

subplot(2,2,1); imagesc([-1 1],[-1 1],atan(0.1*A));
colormap bone
axis xy;
xlabel('Re(z)');
ylabel('Im(z)');

subplot(2,2,2); imagesc([-1 1],[-1 1],atan(0.1*B));
axis xy;
xlabel('Re(z)');
ylabel('Im(z)');

subplot(2,2,3); imagesc([-1 1],[-1 1],atan(0.1*C));
axis xy;
xlabel('Re(z)');
ylabel('Im(z)');

subplot(2,2,4); imagesc([-1 1],[-1 1],atan(0.1*D));
axis xy;
xlabel('Re(z)');
ylabel('Im(z)');