% Oefening 7, vraag 2 en 3

%#ok<*NOPTS>

% author: Tom Gijselinck <tomgijselinck@gmail.com>
% date: 7/10/2014

% vraag 2
% systeem 1
[T,Y] = bwp(5,5,1.25,0.5,1.75,[0,12]);
plot(T,Y(:,1),'-',T,Y(:,2),'--');
title('Oplossing van gedempte trilling, systeem 1');
xlabel('tijd t');
ylabel('oplossing y');
legend('y_1','y_2');

% systeem 2
figure;
[T,Y] = bwp(5,0.625,5,2,0,[0,50]);
plot(T,Y(:,1),'-',T,Y(:,2),'--');
title('Oplossing van gedempte trilling, systeem 2');
xlabel('tijd t');
ylabel('oplossing y');
legend('y_1','y_2');


% vraag 3
syms y dy
% systeem 1
dys1 = [dy,-(5*dy + 1.25*y)/5];
jacs1 = jacobian(dys1, [y,dy]);
eig(jacs1)

% systeem 2
dys2 = [dy,-(0.625*dy + 5*y)/5];
jacs2 = jacobian(dys2, [y,dy]);
eig(jacs2)

% Evaluatie:
% Uit deze resultaten blijkt dat systeem 1 niet stijf is. We zouden dus
% beter de expliciete Runge-Kutta toepassen omdat deze een betere
% nauwkeurigheid biedt dan de trapeziumregel. Systeem 2 is medium stijf en
% dus is de trapeziumregel hier toepasselijk.