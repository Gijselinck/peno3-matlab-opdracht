% Oefening 7, vraag 1

%#ok<*NOPTS>

% author: Tom Gijselinck  <tomgijselinck@gmail.com>
% date: 27/09/2014

% Verklaring keuze numerieke methode:
% We kiezen voor de trapeziumregel omdat deze medium stijf is. De stijfheid
% van het probleem is afhankelijk van de constanten m, k, b. Omdat we deze
% niet kennen bij het oplossen van het probleem werkt de medium stijve
% strapeziumregel het beste. Op deze manier houden we rekening met
% eventuele stijve probleemstellingen zonder te veel rekentijd te
% investeren in de stijve methodes.

function [T,Y] = bwp(m,b,k,y0,v0, tdom)
% Invoer:   constanten m, k en b
%           beginwaarden y0 en v0
%           tijdsinterval tdom (vector)
% Uitvoer:  de oplossingsvectoren Tout en Yout

% Herschrijf het probleem als een lineaire eerste orde stelsel:
% m*y1'' + b*y1' + k*y1 = 0
% wordt
% y1' = y2
% y2' = -(b*y2 + k*y1)/m
% Dit stelsel zetten we in een functie:
    function dy = trilling(t,y) % t wordt niet gebruikt in de berekeningen
        dy = zeros(2,1);
        dy(1) = y(2);
        dy(2) = -(b*y(2) + k*y(1))/m;
    end
%apply solver
[T,Y] = ode23t(@trilling,tdom,[y0,v0]);

end